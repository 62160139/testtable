/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pisit.oxprojectooptesttable;

/**
 *
 * @author User
 */
public class Table {
    private char [][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    private int lastRow;
    private int lastCol;
    private int count = 0;
    public Table(Player x, Player o){
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }
    public void showTable(){
        System.out.println(" 1 2 3");
        for(int i =0; i<table.length; i++){
            System.out.print(i+1 + " ");
            for(int j =0; j<table[i].length; j++){
                System.out.print(table[i][j]);
            }
            System.out.println("");
        }
    }
    public boolean setRowCol(int row, int col){
        if(table[row][col] == '-'){
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            return true;
        }
        return false;
    }
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    public void switchPlayer(){
        if(currentPlayer==playerX){
            currentPlayer = playerO;
        }else{
            currentPlayer = playerX;
        }
    }
    void checkCol(){
        for(int row=0; row<3; row++){
            if(table[row][lastCol] != currentPlayer.getName()){
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }
    void checkRow(){
        for(int col=0; col<3; col++){
            if(table[lastRow][col] != currentPlayer.getName()){
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if(currentPlayer==playerO){
            playerO.win();
            playerX.lose();
        }else{
            playerX.win();
            playerO.lose();
        }
    }
    void checkX(){
        if(table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X'
           || table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X'){
            finish = true;
            winner = currentPlayer;
        }else if(table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O'
           || table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O'){
            finish = true;
            winner = currentPlayer;
        }
    }
    public void checkWin(){
        checkRow();
        checkCol();
        checkDraw();
        checkX();
    } 
     
     void checkDraw(){
        if(count==9){
            finish = true;
            playerO.draw();
            playerX.draw();    
         }
   }
    public boolean isFinish(){
        return finish;
     }
    public Player getWinner(){
        return winner;
     }
    void showBye(){
        System.out.println("Bye bye ...");
    }
    public void Count(){
        count++;
    }


    
}
