/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pisit.oxprojectooptesttable;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Game {
    Scanner kb = new Scanner(System.in);
    char m = '-';
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    public Game(){
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }
    public void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    public void showTable(){
        table.showTable();
    }
    public void input(){
        while(true){
            System.out.println("Plese input Row Col : ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if(table.setRowCol(row, col)){
                table.Count();
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
    }
    public void showTurn(){
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }
    public void newGame(){
        table = new Table(playerX, playerO);
    }
    public void run(){
        this.showWelcome();
        try{
            while(true){
                this.showTable();
                this.showTurn();
                this.input();
                table.checkWin();
                if(table.isFinish()){
                    if(table.getWinner() == null){
                        System.out.println("Draw!!!");
                    }else{
                        System.out.println(table.getWinner() + " Win!!!");
                    }
                    this.showTable();
                    System.out.println("Player again (Y/N)");
                    m = kb.next().charAt(0);
                    if(m == 'Y' || m == 'y'){
                        newGame(); 
                    }else if(m == 'N' || m == 'n'){
                        table.showBye();
                        break;
                    }else{
                        table.showBye();
                        break;
                    }
                
            }
            table.switchPlayer();
        }
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Index out of bound");
        }
        
    }
}
