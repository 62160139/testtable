/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pisit.oxprojectooptesttable;

/**
 *
 * @author User
 */
public class Player {
    private char name;
    private int win; 
    private int lose;
    private int draw;

    public void setName(char name) {
        this.name = name;
    }

    public void win(){
        win++;
    }

    public void lose(){
        lose++;
    }

    public void draw(){
        draw++;
    }

    public char getName() {
        return name;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }
    public Player(char name){
        this.name = name;
    }
}
